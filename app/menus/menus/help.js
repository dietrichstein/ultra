const { release } = require("os");
const { app, shell } = require("electron");

const { getConfig, getPlugins } = require("../../config");
const { arch, env, platform, versions } = process;
const { version } = require("../../package.json");

module.exports = (commands, showAbout) => {
  const submenu = [];

  if (process.platform !== "darwin") {
    submenu.push(
      { type: "separator" },
      {
        role: "about",
        click() {
          showAbout();
        }
      }
    );
  }
  return {
    role: "help",
    submenu
  };
};
