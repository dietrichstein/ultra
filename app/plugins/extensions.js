module.exports = {
  availableExtensions: new Set([
    "onApp",
    "onWindowClass",
    "decorateWindowClass",
    "onWindow",
    "onRendererWindow",
    "onUnload",
    "decorateSessionClass",
    "decorateSessionOptions",
    "middleware",
    "reduceUI",
    "reduceSessions",
    "reduceTermGroups",
    "decorateBrowserOptions",
    "decorateMenu",
    "decorateTerm",
    "decorateUltra",
    "decorateUltraTerm", // for backwards compatibility with ultraterm
    "decorateHeader",
    "decorateTerms",
    "decorateTab",
    "decorateNotification",
    "decorateNotifications",
    "decorateTabs",
    "decorateConfig",
    "decorateKeymaps",
    "decorateEnv",
    "decorateTermGroup",
    "decorateSplitPane",
    "getTermProps",
    "getTabProps",
    "getTabsProps",
    "getTermGroupProps",
    "mapUltraTermState",
    "mapTermsState",
    "mapHeaderState",
    "mapNotificationsState",
    "mapUltraTermDispatch",
    "mapTermsDispatch",
    "mapHeaderDispatch",
    "mapNotificationsDispatch"
  ])
};
