# Ultra

A fork of [Hyper](https://hyper.is) that I created for fixing bugs and testing ideas.

## Contribute

Regardless of the platform you are working on, you will need to have Yarn installed. If you have never installed Yarn before, you can find out how at: https://yarnpkg.com/en/docs/install.

### Windows Build Notes

1. Install the dependencies: `yarn`
2. Run  `yarn global add windows-build-tools` from an elevated prompt (as an administrator) to install `windows-build-tools`
2. Fork this repository to your own GitLab account and then clone to your local device
  
### macOS Build Notes

### General Build Notes

1. Build the code and watch for changes: `yarn run dev`
2. Run `yarn run app` from another terminal tab/window/pane
3. If you are using **Visual Studio Code**, select `Launch Ultra` in debugger configuration to launch a new Ultra instance with debugger attached.
4. If you interrupt `yarn run dev`, you'll need to relaunch it each time you want to test something. Webpack will watch changes and will rebuild renderer code when needed (and only what have changed). You'll just have to relaunch electron by using yarn run app or VSCode launch task.
5. To make sure that your code works in the finished application, you can generate the binaries in the `./dist` directory like this: `yarn run dist`

### Known Issues

#### Error building `node-pty`

If after building during development you get an alert dialog related to `node-pty` issues,
make sure its build process is working correctly by running `yarn run rebuild-node-pty`.

If you are on macOS, this typically is related to Xcode issues (like not having agreed
to the Terms of Service by running `sudo xcodebuild` after a fresh Xcode installation).

#### Error with `codesign` on macOS when running `yarn run dist`

If you have issues in the `codesign` step when running `yarn run dist` on macOS, you can temporarily disable code signing locally by setting
`export CSC_IDENTITY_AUTO_DISCOVERY=false` for the current terminal session.
